package hu.uniobuda.nik.SpACE;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameView extends View {
    Bitmap bitmap;
    Bitmap BombBitmap;
    Bitmap tnt;
    Rect background;
    int backgroundWidth, backgroundHeight;
    GameUser theRealPlayer;
    private ArrayList<GamePlayer> gameplayers;
    Handler handler;
    Runnable runnable;
    int numbersofEnemy;
    Bitmap spaceship;
    private ArrayList<GamePlayer> healts;
    boolean jatekvege;

    public GameView(final Context context, final int width, final int height, final int level) {
        super(context);
        // a háttérkép
        backgroundWidth = width;
        backgroundHeight = height;

        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.start_background);

        spaceship=BitmapFactory.decodeResource(getResources(), R.drawable.spaceship);
        spaceship = spaceship.createScaledBitmap(spaceship, backgroundWidth/10, backgroundHeight/7, false);

        tnt=BitmapFactory.decodeResource(getResources(), R.drawable.grenade);
        tnt = tnt.createScaledBitmap(tnt, backgroundWidth/15, backgroundWidth/15, false);

        BombBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.bomb);
        BombBitmap = BombBitmap.createScaledBitmap(BombBitmap, backgroundWidth/15, backgroundWidth/15, false);

        // lekéri a méretét a képernyőnek
        numbersofEnemy = level;

        // teljesképernyős legyen a háttér
        background = new Rect(0, 0, backgroundWidth, backgroundHeight);
        theRealPlayer = new GameUser(context, "Feri", backgroundWidth, backgroundHeight, spaceship, level);
        gameplayers = new ArrayList<GamePlayer>();

        Random rnd = new Random();
        Bitmap enemy = BitmapFactory.decodeResource(context.getResources(), R.drawable.ufo);
        enemy = enemy.createScaledBitmap(enemy, backgroundWidth/7, backgroundWidth/7, false);
        for (int i = 0; i < numbersofEnemy; i++) {
            gameplayers.add(new GamePlayer(enemy, width, rnd.nextInt(height-height/3)+height/17, level));
        }

        Bitmap healt = BitmapFactory.decodeResource(context.getResources(), R.drawable.like);
        healt = healt.createScaledBitmap(healt, backgroundWidth/17, backgroundWidth/17, false);
        healts = new ArrayList<GamePlayer>();
        for (int i = 0; i < theRealPlayer.getHealth(); i++) {
            healts.add(new GamePlayer(healt, i * width / theRealPlayer.getHealth(), 15, level));
        }

        jatekvege = false;
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!jatekvege) {
                    Random rnd = new Random();

                    ArrayList<BulletObject> destroyedBullets = new ArrayList<BulletObject>();
                    ArrayList<GamePlayer> destroyedEnemy = new ArrayList<GamePlayer>();
                    for (BulletObject bullet : theRealPlayer.Bullets()) {// az összes felhasználó által lőtt golyón végignézzük hogy eltalált-e valakit
                        if (!destroyedBullets.contains(bullet)) {
                            if (bullet.getY() + bullet.getBottom() > 0) {
                                bullet.setY(-15);
                            } else {

                                destroyedBullets.add(bullet);
                            }
                            for (int i = 0; i < gameplayers.size(); i++) {// az összes enemyt-t végignézi hogy ütközött-e
                                if (destroyedEnemy.contains(gameplayers.get(i)))//ha már megsemmisült akkor ne kapjon el lövedéket
                                    continue;
                                if (UtkozesVizsgalat(bullet, gameplayers.get(i))) {
                                    destroyedBullets.add(bullet);
                                    destroyedEnemy.add(gameplayers.get(i));
                                    /*gameplayers.get(i).setHealth(bullet.getDamage());
                                    if (gameplayers.get(i).getHealth() <= 0) {
                                        destroyedEnemy.add(gameplayers.get(i));
                                    }*/
                                }
                            }
                        }
                    }

                    for (GamePlayer gp : gameplayers//enemyken
                            ) {
                        if (gp.getX() >= 0 - gp.getRightCorner()) {
                            gp.setX(gp.getX() - gp.getSpeed());
                        } else {
                            gp.setX(width + gp.getX());
                        }
                        for (BulletObject bullet : gp.Bullets()) {//minden enemy lövedéein végigmegyünk
                            if (!destroyedBullets.contains(bullet)) {
                                if (bullet.getY() < backgroundHeight) {
                                    bullet.setY(7);
                                } else {

                                    destroyedBullets.add(bullet);
                                }

                                if (UtkozesVizsgalat(bullet, theRealPlayer)) {
                                    destroyedBullets.add(bullet);
                                    theRealPlayer.setHealth(theRealPlayer.getHealth() - 1);
                                    healts.remove(healts.get(healts.size() - 1));
                                    if (theRealPlayer.getHealth() <= 0) {
                                        destroyedEnemy.add(theRealPlayer);
                                    }
                                }
                            }
                        }
                    }

                    if (theRealPlayer.getHealth() <= 0 || gameplayers.size() == 0) { //játékvége
                        jatekvege = true;
                    }

                    for (int i = 0; i < gameplayers.size(); i++) {
                        for (int j = 0; j < gameplayers.get(i).Bullets().size(); j++) {
                            if (destroyedBullets.contains(gameplayers.get(i).Bullets().get(j))) {
                                gameplayers.get(i).Bullets().remove(j);//töröljük a lövedéeket
                            }
                        }
                        if (destroyedEnemy.contains(gameplayers.get(i)))
                            gameplayers.remove(gameplayers.get(i));//töröljük az enemyt
                    }
                    for (int i = 0; i < theRealPlayer.Bullets().size(); i++) {
                        if (destroyedBullets.contains(theRealPlayer.Bullets().get(i))) {
                            theRealPlayer.Bullets().remove(i);//töröljük a lövedéeket
                        }
                    }

                    for (GamePlayer gp : gameplayers
                            ) {
                        if (rnd.nextDouble() * 10000 / level < 1) {
                            gp.Shot(tnt);
                        }
                    }
                    invalidate();
                }
            }
        };
    }

    private boolean UtkozesVizsgalat(BulletObject bullet, GamePlayer player){
        //if (bullet.getX()<=||bullet.)
        return intersects(bullet, player);
        //return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // háttér kirajzolása
        canvas.drawBitmap(bitmap,null,background,null);
        canvas.drawBitmap(theRealPlayer.getBitmap(),theRealPlayer.getX(),theRealPlayer.getY(), null);
        for (BulletObject bullet: theRealPlayer.Bullets()
             ) {
            canvas.drawBitmap(bullet.getBitmap(),bullet.getX(),bullet.getY(), null);
        }
        for (GamePlayer gp:gameplayers
             ) {
            canvas.drawBitmap(gp.getBitmap(),gp.getX(),gp.getY(), null);
            for (BulletObject bullet:gp.Bullets()
                 ) {
                canvas.drawBitmap(bullet.getBitmap(),bullet.getX(),bullet.getY(), null);
            }
        }
        for (GamePlayer healt : healts) {
            canvas.drawBitmap(healt.getBitmap(), healt.getX(), healt.getY(), null);
        }
        handler.postDelayed(runnable,0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x=event.getX();
        float y=event.getY();
        // majd gombokhoz kell
        int idx=event.getAction();

        if (idx==MotionEvent.ACTION_DOWN)
        {
            theRealPlayer.Shot(BombBitmap);
        }
        return true;
    }

     public boolean intersects(BulletObject bo, GamePlayer gp)
    {

        Rect rbullet = new Rect(bo.getX(),bo.getY(),bo.getRightCorner(),bo.getBottom());
        Rect rufo = new Rect(gp.getX(),gp.getY(),gp.getRightCorner(),gp.getBottom());

        boolean collision = false;
        collision = rufo.intersect(rbullet);
        return collision;


    }
}
