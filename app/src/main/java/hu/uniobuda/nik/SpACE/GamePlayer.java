package hu.uniobuda.nik.SpACE;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;
import java.util.Random;

public class GamePlayer {
    private int x, y, speed, health, damage;
    private Bitmap bitmap;
    private Random rnd;
    private ArrayList<BulletObject> bullets;

    public GamePlayer(Bitmap bitmap, int x, int y, int level) {
        rnd=new Random();
        this.speed=rnd.nextInt(level)+5;
        this.bitmap = bitmap;
        this.bullets=new ArrayList<BulletObject>(5);
        this.x = x;
        this.y = y;
    }
    public int getSpeed(){
        return  this.speed;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x =x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

//    public void setBitmap(Bitmap bitmap) {
//        this.bitmap = bitmap;
//    }
//
    public int getRightCorner(){
        return this.x+this.bitmap.getWidth();
    }

    public int getBottom(){
        return this.y+this.bitmap.getHeight();
    }

    public ArrayList<BulletObject> Bullets(){
        return this.bullets;
    }





    public void Shot(Bitmap bitmap){
        bullets.add( new BulletObject(this.x,this.y,this.speed, this.damage, bitmap));
    }
}
