package hu.uniobuda.nik.SpACE;

import android.content.Intent;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor gyroscopeSensor;
    private SensorEventListener gyroscopeEventListener;
    private int level;

    private GameView gameView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int width = displayMetrics.widthPixels;
        final int height=displayMetrics.heightPixels;
        if (SettingsActivity.mode.equals("Normal"))

        {
            level = 20;

        }
        else if (SettingsActivity.mode.equals("Easy"))
        {
            level =10;

        }
        else
        {
            level = 30;

        }
        gameView=new GameView(this,width,height,level);

        //setContentView(R.layout.activity_game);


        //Notification és Action bar eltüntetése
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Szenzor inicializálása
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // spaceship = findViewById(R.id.spaceship);



        if (gyroscopeSensor == null) {
            Toast.makeText(this, "Nem támogatott!", Toast.LENGTH_LONG).show();
        }
        //Játékos hajójának mozgatása
        gyroscopeEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (sensorEvent.values[0] > 0.5 ) {
                    gameView.theRealPlayer.setX(gameView.theRealPlayer.getX() -5);
                }
                else {
                    if (sensorEvent.values[0] < -0.5 ) {
                        gameView.theRealPlayer.setX(gameView.theRealPlayer.getX()+5);
                    }
                }
            }
            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {}
        };
        setContentView(gameView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(gyroscopeEventListener, gyroscopeSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(gyroscopeEventListener);
    }
}
