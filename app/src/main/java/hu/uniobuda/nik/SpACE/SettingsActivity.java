package hu.uniobuda.nik.SpACE;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    private Button okButton;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    static String mode = "Easy";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        addListenerOnButton();


    }
    public void addListenerOnButton() {

        radioGroup = (RadioGroup) findViewById(R.id.radio);
        okButton = (Button) findViewById(R.id.OkBt);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = 0;

                // get selected radio button from radioGroup
                if (radioGroup.getCheckedRadioButtonId() != -1)
                {
                    selectedId = radioGroup.getCheckedRadioButtonId();
                }

                // find the radiobutton by returned id
                if (selectedId != 0) {
                    radioButton = (RadioButton) findViewById(selectedId);
                    SettingsActivity.mode =  radioButton.getText().toString();
                }


                Toast.makeText(SettingsActivity.this,
                           mode +" mode selected!", Toast.LENGTH_SHORT).show();


                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                finish();

            }

        });

    }
}
