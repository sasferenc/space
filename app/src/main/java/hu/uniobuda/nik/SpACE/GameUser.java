package hu.uniobuda.nik.SpACE;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class GameUser extends GamePlayer {
    private String name;
    private int width;
    public GameUser(Context context,String name, int width,int height, Bitmap bitmap, int level) {
        super(bitmap, width, height, level);

        this.name=name;
        this.width=width;

        super.setX(width/2-bitmap.getWidth()/2);

        this.setDamage(100);
        this.setHealth(5);
        this.setY(height-this.getBitmap().getHeight());
    }

    @Override
    public void setX(int x) {
        if (x>=0&&x+this.getBitmap().getWidth()<=width)
            super.setX(x);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
    }
}
