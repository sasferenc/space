package hu.uniobuda.nik.SpACE;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import hu.uniobuda.nik.SpACE.R;
public class BulletObject {

    private Bitmap bitmap;
    private int x, y, speed, damage;

    public BulletObject(int x, int y, int speed, int damage,Bitmap bitmap) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.damage = damage;
        this.bitmap=bitmap;
    }

    public int getY() {
        return y;
    }

    public int getSpeed() {
        return speed;
    }

    public int getDamage() {
        return damage;
    }
    public void setY(int y){
        this.y=this.y+y;
    }
    public int getX(){
        return this.x;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public int getRightCorner(){
        return this.x+this.bitmap.getWidth();
    }

    public int getBottom(){
        return this.y+this.bitmap.getHeight();
    }
}
